package com.paises;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import com.paises.beans.Ciudad;
import com.paises.beans.Estado;
import com.paises.beans.Pais;

public class Inicio {

	private Pais pais;

	public static void main(String[] args) {

		Inicio sist = new Inicio();
		// Iniciando datos
		sist.iniciarDatos();

		try (Scanner sc = new Scanner(System.in);) {

			int opcion = 0;

			do {
				sist.mostrarMenu();
				opcion = sc.nextInt();
				if (opcion != 0) {
					
					switch (opcion) {
					case 1:
						sist.ciudadConMayorPoblacion();
						break;
					case 2:
						sist.menorPoblacionPorEstado();
						break;
					case 3:
						sist.promedioPoblacionEstado();
						break;
					case 4:
						System.out.println("Rango menor");
						int inicio = sc.nextInt();
						System.out.println("Rango mayor");
						int fin = sc.nextInt();
						sist.porcentajePorRango(inicio, fin);
						break;
					case 5:
						sist.porcentajeMayorQueElPromedio();
						break;
					default:
						System.out.println("Opci�n no valida");
						break;
					}
				}

			} while (opcion != 0);

		} catch (Exception e) {
			System.out.println("Caracter no valido");
		}

		System.out.println("Fin");

	}

	private void iniciarDatos() {
	
		Ciudad medellin = new Ciudad("Medellin", 1000);
		Ciudad envigado = new Ciudad("Envigado", 200);
		Ciudad bogota = new Ciudad("Bogota", 3000);
		Ciudad cali = new Ciudad("Cali", 800);
	
		// ANTIOQUIA
		Estado antioquia = new Estado("Antioquia");
		List<Ciudad> ciudadesAntioquia = new ArrayList<Ciudad>(Arrays.asList(medellin, envigado));
		antioquia.setCiudades(ciudadesAntioquia);
	
		// VALLE DEL CAUCA
		Estado valleDelCauca = new Estado("Valle del cauca");
		List<Ciudad> ciudadesValle = new ArrayList<Ciudad>(Arrays.asList(cali));
		valleDelCauca.setCiudades(ciudadesValle);
	
		// CUNDINAMARCA
		Estado cundinamarca = new Estado("Cundinamarca");
		List<Ciudad> ciudadesCundinamarca = new ArrayList<Ciudad>(Arrays.asList(bogota));
		cundinamarca.setCiudades(ciudadesCundinamarca);
	
		// Se crea pais
		pais = new Pais();
		pais.setNombre("Colombia");
		List<Estado> listaEstados = new ArrayList<Estado>(Arrays.asList(antioquia, valleDelCauca, cundinamarca));
		pais.setEstados(listaEstados);
	}

	private void ciudadConMayorPoblacion() {

		Ciudad mayor = null;
		for (Estado estado : pais.getEstados()) {
			for (Ciudad ciudad : estado.getCiudades()) {
				if (mayor == null) {
					mayor = ciudad;
					continue;
				}

				if (ciudad.getPoblacion() > mayor.getPoblacion())
					mayor = ciudad;
			}
		}
		System.out.println(
				"La ciudad con mayor poblaci�n es " + mayor.getNombre() + ", " + mayor.getPoblacion() + " habitantes");
	}

	private void menorPoblacionPorEstado() {

		for (Estado estado : pais.getEstados()) {

			Ciudad menorPorEstado = null;
			for (Ciudad ciudad : estado.getCiudades()) {
				if (menorPorEstado == null) {
					menorPorEstado = ciudad;
					continue;
				}

				if (ciudad.getPoblacion() < menorPorEstado.getPoblacion())
					menorPorEstado = ciudad;

			}
			System.out.println("La menor poblaci�n del estado " + estado.getNombre() + " es "
					+ menorPorEstado.getNombre() + ", habitantes " + menorPorEstado.getPoblacion());
		}
	}

	private void promedioPoblacionEstado() {

		for (Estado estado : pais.getEstados()) {

			long sumaPorEstado = 0;
			for (Ciudad ciudad : estado.getCiudades())
				sumaPorEstado += ciudad.getPoblacion();

			float promedio = sumaPorEstado / estado.getCiudades().size();
			System.out.println("Promedio " + estado.getNombre() + ": " + promedio);
		}

	}

	private void porcentajePorRango(int inicio, int fin) {

		for (Estado estado : pais.getEstados()) {

			float aplicaRango = 0;
			for (Ciudad ciudad : estado.getCiudades())
				if (ciudad.getPoblacion() >= inicio && ciudad.getPoblacion() <= fin)
					aplicaRango++;

			float porcentaje = (aplicaRango / estado.getCiudades().size()) * 100;
			System.out.println("El " + porcentaje + "% de " + estado.getNombre() + " esta en dicho rango");

		}

	}

	private void porcentajeMayorQueElPromedio() {

		int totalCiudades = 0;
		float poblacionTotal = 0;
		for (Estado estado : pais.getEstados()) {
			for (Ciudad ciudad : estado.getCiudades()) {
				poblacionTotal += ciudad.getPoblacion();
				totalCiudades++;
			}
		}
		float promedio = poblacionTotal / totalCiudades;

		float mayoresPromedio = 0;
		for (Estado estado : pais.getEstados()) {
			for (Ciudad ciudad : estado.getCiudades()) {
				if (ciudad.getPoblacion() > promedio) {
					mayoresPromedio++;
				}
			}
		}

		float porcentaje = (mayoresPromedio / totalCiudades) * 100;
		System.out.println("El " + porcentaje + "% de las ciudades estan por encima del promedio (" + promedio + ")");

	}

	private void mostrarMenu() {
		System.out.println("");
		System.out.println("***************************************************************");
		System.out.println("* SELECCIONE LA OPCI�N QUE DESEA VER                          *");
		System.out.println("* 1 = Ciudad con mayor poblaci�n                              *");
		System.out.println("* 2 = Ciudad con menor poblaci�n                              *");
		System.out.println("* 3 = Promedio de poblaci�n por estado                        *");
		System.out.println("* 4 = Porcentaje de ciudades en un rango                      *");
		System.out.println("* 5 = Porcentaje de ciudades por encima del promedio          *");
		System.out.println("* 0 = Salir del programa                                      *");
		System.out.println("***************************************************************");
	}

}
