package com.paises.beans;

import java.util.List;

public class Pais {

	private String nombre;
	private List<Estado> estados;

	public Pais() {
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Estado> getEstados() {
		return estados;
	}

	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}

	@Override
	public String toString() {
		return "Pais [nombre=" + nombre + ", estados=" + estados + "]";
	}

}
