package com.paises.beans;

public class Ciudad {

	private String nombre;
	private long poblacion;

	public Ciudad(String nombre, long poblacion) {
		super();
		this.nombre = nombre;
		this.poblacion = poblacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public long getPoblacion() {
		return poblacion;
	}

	public void setPoblacion(long poblacion) {
		this.poblacion = poblacion;
	}

	@Override
	public String toString() {
		return "Ciudad [nombre=" + nombre + ", poblacion=" + poblacion + "]";
	}

}
